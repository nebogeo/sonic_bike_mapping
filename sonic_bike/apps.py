from importlib import import_module

from django.apps import AppConfig as BaseAppConfig


class AppConfig(BaseAppConfig):

    name = "sonic_bike"

    def ready(self):
        import_module("sonic_bike.receivers")
